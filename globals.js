global.app_env = process.env.ENV

global.rootDir = __dirname
global.__proto__.mrequire = require.main.require
global.config = require('./config')

global.logLevel = parseInt(process.env.LOGLEVEL ?? '0')

if(app_env === 'production') {
  const order_notify = mrequire("./services/order_notify_mailer")
  const order_sync = mrequire("./services/sync-order")
  process.on("new_order", order => {
    order_notify(order)
    order_sync({action: "new", ...order})
  })
  process.on("update_order", order => { order_sync({action: "update", ...order}) })
}


function Log (level, ...args) {
  if(logLevel < level) {
    console.log(...args)
  }
}

global.log = {
  write: Log,
  system () {
    Log(0, '>> System |', ...arguments)
  },
  debug () {
    Log(1, '>> Debug |', ...arguments)
  },
  info () {
    Log(2, '>> Info |', ...arguments)
  },
  warn () {
    Log(3, '>> Warn |', ...arguments)
  },
  error () {
    Log(4, '>> Error |', ...arguments)
  }
}