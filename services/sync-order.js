"use strict"

const { tvc_api } = config.services
const request = mrequire('./modules/http2-client')(tvc_api)

module.exports = function ({action, uid, order_id}) {
  request(`/api/sync/order/${uid}`)
    .then(() => {
      log.info(`sync ${action} order ${order_id} success`)
    })
    .catch((err) => {
      log.error(err.message + "\n", `sync ${action} order ${order_id} failed`)
    })
}