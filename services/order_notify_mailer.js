const axios = require("axios").create(config.services.notify_mailer)
const FormData = require("form-data")

const template = order => `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <style>
    body {
      font-size: 25px;
      margin: 0;
      padding: 0;
      font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
    }
    .header {
      background-color: #dcdde1;
      padding: 15px 50px;
      text-align: left;
      font-size: 28px;
      font-weight: bold;
      color:#718093;
    }
    table {
      padding: 30px 50px;
      color: #000;
    }
    .title {
      color: #2980b9;
      font-weight: bold;
      padding: 10px 20px 10px 0px;
      width: 300px;
    }
    .address {
      vertical-align: top;
    }
    td > p {
      margin-top: 0px;
      padding-top: 10px;
    }
  </style>
</head>
<body>
<div colspan="2" class="header">Thông tin đơn hàng</div>
<table cellpadding="5" cellspacing="0" width="100%">
  <tr>
    <td class="title">Mã đơn hàng:</td>
    <td class="content">${order.order_id}</td>
  </tr>
  <tr>
    <td class="title">Số tiền:</td>
    <td class="content">${order.total_pay}</td>
  </tr>
  <tr>
    <td class="title">Phương thức thanh toán:</td>
    <td class="content">${order.pay_gateway}</td>
  </tr>
  <tr>
    <td class="title">Voucher code:</td>
    <td class="content">${order.voucher_code}</td>
  </tr>
  <tr>
    <td class="title address">
      Địa chỉ nhận hàng:
    </td>
    <td class="content">
      <p>${order.customer_name}</p>
      <p>${order.address_des}</p>
    </td>
  </tr>
  <tr>
    <td class="title">Số điện thoại:</td>
    <td class="content">${order.phone_number}</td>
  </tr>
</table>
</body>
</html>`

const parseOrderInfo = order => {
  if(order.pay_gateway === 'cod') {
    order.pay_gateway = "Trả tiền khi giao hàng"
  } else {
    order.pay_gateway = `Thanh toán trực tuyến qua ${order.pay_gateway}`
  }
  return order
}

module.exports = function (order) {
  if((/test/gi).test(order.customer_name))
    return // Nếu tên customer cớ chữ test thì không gởi mail
  const formData = new FormData()
  formData.append("fromName", "New Order Notify")
  formData.append("subject", `[Đơn hàng] Thông tin đơn hàng ${order.order_id}`)
  formData.append("content", template(parseOrderInfo({...order})))
  axios.post("/", formData, { headers: formData.getHeaders() }).then(res => {
    log.info("notify new order", res.data)
  }).catch(err => {
    log.error("notify new order", err.response?.data || err.message)
  })
}