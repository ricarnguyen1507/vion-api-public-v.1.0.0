const { getService } = mrequire('./modules/dgraph-client')
const { alpha_grpc } = config.services

const tvcdb = getService('tvcdb', alpha_grpc)


function addType (type, data) {
  if(data) {
    if(Array.isArray(data)) {
      data.forEach(o => {
        o['dgraph.type'] = type
      })
    } else if(typeof data === 'object') {
      data['dgraph.type'] = type
    }
  }
  return data
}
function addTypeSet (type, data) {
  if(data.set) {
    addType(type, data.set)
  }
  return data
}
tvcdb.addTypeSet = addTypeSet
tvcdb.addType = addType
tvcdb.getUids = function (result, initData) {
  const uid_arr = result.getUidsMap().arr_
  if(uid_arr.length) {
    const uids = {}
    uid_arr.forEach(([k, v]) => {
      uids[k] = v
    })
    return {
      ...initData,
      ...(uid_arr.length && {uids})
    }
  }
  return initData
}

module.exports = tvcdb