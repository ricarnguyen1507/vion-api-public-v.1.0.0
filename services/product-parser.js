"use strict"

const { safeParseJson, handleUrlDescHtml } = mrequire('./utils')

const hasNull = /null/i

function parseWith (obj, prop, parser) {
  if(prop in obj) obj[prop] = parser(obj[prop])
}

function numberWithCommas (x, v) {
  return x ? x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") : v
}

function capitalize (str = '', v = '') {
  return str ? str.toLowerCase().replace(/^([a-z])|\s+([a-z])/g, v => v.toUpperCase()) : v
}

function getPrice (product) {
  const listed_price_with_vat = product['product.pricing']?.listed_price_with_vat ?? 0
  const price_with_vat = product['product.pricing']?.price_with_vat ?? 0
  const result = listed_price_with_vat - price_with_vat
  return numberWithCommas(result, 'null')
}
function getPercent (product) {
  const listed_price_with_vat = product['product.pricing']?.listed_price_with_vat ?? 0
  const price_with_vat = product['product.pricing']?.price_with_vat ?? 0
  const result = (listed_price_with_vat - price_with_vat) / listed_price_with_vat * 100
  return result ? Math.ceil(result) : 'null'
}

function getPromo (product) {
  return capitalize(product.promotion_desc, 'null')
}
function getOrigin (product) {
  return capitalize(product.original, 'null')
}
function getBrand (product) {
  return capitalize(product['product.brand']?.brand_name, 'null')
}
function getSupplier (product) {
  return capitalize(product['product.supplier']?.supplier_name, 'null')
}
function getShelfLife (product) {
  return capitalize(product['shelf_life'], 'null')
}
function getBrandShopName (product) {
  // console.log('get brand shop name', product['brand_shop'][0]?.display_name_in_product)
  if(product?.brand_shop?.length) {
    return capitalize(product['brand_shop'][0]?.display_name_in_product, 'null')
  } else {
    return ''
  }
}
function getWarranty (product) {
  if(product?.warranty_policy) {
    return capitalize(product?.warranty_policy, 'null')
  } else {
    return ''
  }

}

function parseStringDescription (str = '', product) {
  if(!str) return ''

  //Price
  str = str.replace(/{price}/g, getPrice(product))
  // percent
  str = str.replace(/{percent}/g, getPercent(product))
  //origin - Xuất xứ
  str = str.replace(/{origin}/g, getOrigin(product))
  //origin - Brand nhà sản xuất
  str = str.replace(/{brand}/g, getBrand(product))
  //origin - Nhà cung cấp
  str = str.replace(/{supplier}/g, getSupplier(product))
  // Khuyến mãi
  str = str.replace(/{promo}/g, getPromo(product))
  // Hạn sử dụng
  str = str.replace(/{thsd}/g, getShelfLife(product))

  // Tên hiển thị trong sản phẩm
  str = str.replace(/{brandshopname}/g, getBrandShopName(product))

  // Tên hiển thị trong sản phẩm
  str = str.replace(/{warranty}/g, getWarranty(product))

  if(hasNull.test(str)) return ''

  return str
}


module.exports = function (products = []) {
  if(Array.isArray(products)) {
    for(let prd of products) {
      if("previews" in prd) {
        let mtype = '',
          images = [],
          videos = []
        for(let prv of prd.previews) {
          mtype = prv.media_type
          if(mtype === 'image')
            images.push(prv)
          else if(mtype === 'video') {
            videos.push(prv)
          }
        }
        delete prd.previews
        prd.images = images
        prd.videos = videos
      }

      parseWith(prd, 'description', safeParseJson)
      prd['description_html'] = handleUrlDescHtml(prd['description_html'], config.serve.image_url)

      prd['product.tag']?.forEach(tag => {
        if(tag.tag_category === 'name_tag') {
          tag['name_tag_value_1'] = parseStringDescription(tag['name_tag_value_1'], prd)
          tag['name_tag_value_2'] = parseStringDescription(tag['name_tag_value_2'], prd)
          delete tag.name_tag_value
        }
      })

      const short_descriptions = {}
      for (let idx = 1; idx <= 4; idx++) {
        const shortDes = prd[`short_description_${idx}`]
        if(shortDes) {
          short_descriptions[`short_des_${idx}`] = parseStringDescription(shortDes.short_des, prd)
          short_descriptions[`short_des_${idx}_color`] = shortDes.short_des_color ?? '#000'
        }
        delete prd[`short_description_${idx}`]
      }

      if(!Object.keys(short_descriptions).length) {
        const short_descriptions_prd = prd?.short_descriptions ?? {}
        if (short_descriptions_prd?.short_des_1?.length) {
          short_descriptions_prd.short_des_1 = parseStringDescription(short_descriptions_prd.short_des_1, prd)
        }
        if (short_descriptions_prd?.short_des_2?.length) {
          short_descriptions_prd.short_des_2 = parseStringDescription(short_descriptions_prd.short_des_2, prd)
        }
        if (short_descriptions_prd?.short_des_3?.length) {
          short_descriptions_prd.short_des_3 = parseStringDescription(short_descriptions_prd.short_des_3, prd)
        }
        if (short_descriptions_prd?.short_des_4?.length) {
          short_descriptions_prd.short_des_4 = parseStringDescription(short_descriptions_prd.short_des_4, prd)
        }
        prd.short_descriptions = short_descriptions_prd
      }else {
        prd.short_descriptions = short_descriptions
      }
    }
  }
  return products
}