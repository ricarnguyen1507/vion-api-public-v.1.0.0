const axios = require("axios").create(config.services.order_id_generator)
module.exports = async function () {
  return (await axios.get("/")).data
}