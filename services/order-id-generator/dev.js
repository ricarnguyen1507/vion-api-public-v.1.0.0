"use strict"

const db = mrequire('./services/tvcdb')

function parseCurrentDate () {
  const date = new Date()
  const year = String(date.getFullYear()).slice(-2)
  const month = String(date.getMonth() + 1).padStart(2, "0")
  const day = String(date.getDate()).padStart(2, "0")
  return `${year}${month}${day}`
}

async function getNextIndayCount (today) {
  let {orders} = await db.query(`{ orders(func: has(order_id), orderdesc: order_id, first: 1) @filter(type(PrimaryOrder)){order_id} }`)
  if(orders.length === 1) {
    const [dateStr, count] = orders[0].order_id.match(/^S(\d{6})-(\d{6})/).slice(1)
    if(dateStr === today) {
      return +count + 1
    }
  }
  return 1
}

async function generateOrderId () {
  const today = parseCurrentDate()
  const inDayCount = await getNextIndayCount(today)
  return `S${today}-${String(inDayCount).padStart(6, "0")}`
}

module.exports = generateOrderId