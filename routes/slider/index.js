const db = mrequire('./services/tvcdb')
async function getSliders(){
  let { result } = await db.query(`{
    result(func: type(Slider)) {
        uid
        img
        title
        header
    }
  }`)
  return {
    statusCode: 200,
    data: result
  }
}
module.exports = [
  ['get', '/list/slider', getSliders],
]