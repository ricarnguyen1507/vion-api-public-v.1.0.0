const filterCollCantShow = (items) => {
  let filterResult = []
  items.forEach(r => {
    let item_had_prod = false
    if (r?.children?.length > 0) {
      r.children.map(rc => {
        if (rc?.items?.length > 0) {
          item_had_prod = true
        }
      })
    }

    if (item_had_prod) {
      filterResult.push({
        uid: r.uid,
        collection_name: r.collection_name,
        collection_image: r.collection_image,
        highlight_name: r.highlight_name,
        collection_icon: r.collection_icon,
        color_title: r.color_title,
        gradient_start: r.gradient_start,
        gradient_end: r.gradient_end,
        layout_type: r.layout_type,
      })
    }

  })

  return filterResult
}

module.exports = {
  filterCollCantShow
}