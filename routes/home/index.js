const db = mrequire('./services/tvcdb')
const prodFields = mrequire('./fragments/product')
const prodFilter = mrequire('./fragments/productFilter')
const { isUid, safeParseJson } = mrequire('./utils')
const parseSections = require("./parseSections")
const parseSectionsWithRegion = require("./parseSectionsWithRegion")
const {getCusRegion} = mrequire('./modules/region')
const layoutFields =
`uid
layout_name
image_cover
target_type
layout.layout_section @facets(orderasc: display_order) {
  uid
  section_value
  section_name
  section_ref
  section_type
  section_limit
}
layout.hybrid_layout @filter(eq(display_status, 2)) {
  uid
  layout_name
  layout.layout_section @facets(orderasc: display_order) {
    uid
    section_value
    section_name
    section_ref
    section_type
    image
  }
}
`

async function homeHandle ({query = {}, body = {}, headers = {}}) {
  const { customer_id, uids = [] } = {...query, ...body, ...headers}

  if(customer_id && !(Array.isArray(uids) && [customer_id, ...uids].every(uid => isUid(uid)))) {
    throw new Error('Invalid parameters')
  }
  let layout,
    customer_region = null
  if (customer_id && isUid(customer_id)) {
    layout = await getPriorityLayout(customer_id)
    customer_region = await getCusRegion(customer_id)
  } else {
    let query = `{
      layouts(func: type(Layout)) @filter(eq(is_default, true) and not has(~brand_shop.layout)) {
        ${layoutFields}
      }
    }`
    layout = (await db.query(query)).layouts
  }
  const random = Math.floor(Math.random() * layout.length)
  let selectedSection = []
  if (customer_region && isUid(customer_region)) {
    selectedSection = await parseSectionsWithRegion(layout[random], uids, customer_id, customer_region)
  } else {
    selectedSection = await parseSections(layout[random], uids, customer_id)
  }

  if (selectedSection?.['layout.hybrid_layout']?.['layout.layout_section']?.length > 0) {
    const hybrid = selectedSection['layout.hybrid_layout']
    let hybridSection = {
      'uid': hybrid.uid,
      'section_value': "hybrid_section",
      'section_type': "hybrid_section",
      'section_name': hybrid.layout_name,
      'hybrid_section': []
    }
    if (hybrid?.['layout.layout_section']?.length > 0) {
      const section = hybrid['layout.layout_section']
      for(let i = 0; i < section.length; i++) {
        if (section[i]['section_value'] === "landing_page" && section[i]['section_ref'] && section[i]['section_ref'] !== "") {
          const { landing_page: [landingPage] } = await db.query(`{
            landing_page(func: uid(${section[i]['section_ref']})) @filter(type(LandingPage) AND eq(display_status, 2)) {
              uid
              landing_name
              image_cover
            }
          }`)

          if (landingPage?.['image_cover']) {
            section[i]['image_background'] = landingPage['image_cover']
          }
        }
        hybridSection.hybrid_section.push(section[i])
      }
    }
    selectedSection['layout.layout_section'].unshift(hybridSection)
    delete selectedSection['layout.hybrid_layout']
  }
  return {
    statusCode: 200,
    data: selectedSection
  }
}

async function getPriorityLayout (customer_id) {
  let listLayout
  let query = `{
    var(func: uid(${customer_id})) {
      uid
      group_cus as ~group_customer.customers
      lay_cus as ~layout.customers @filter(not eq(is_deleted, true) AND eq(display_status, 2) AND eq(target_type, 1))
    }
    var(func: uid(group_cus)) {
      lay_group as ~layout.group_customers @filter(not eq(is_deleted, true) AND eq(display_status, 2) AND eq(target_type, 0))
    }
    lay_all as allCustomer(func: type(Layout)) @filter(NOT has(~brand_shop.layout) AND NOT eq(is_deleted, true) AND eq(target_type, 2) AND eq(display_status, 2))

    layoutCus(func: uid(lay_cus)) @filter(NOT has(~brand_shop.layout)) {
      ${layoutFields}
    }
    layoutGroup(func: uid(lay_group)) @filter(NOT has(~brand_shop.layout)) {
      ${layoutFields}
    }
    layoutAll(func: uid(lay_all)) @filter(NOT has(~brand_shop.layout)) {
      ${layoutFields}
    }
  }`
  listLayout = await db.query(query)
  if(listLayout.layoutCus.length) {
    return listLayout.layoutCus
  } else if(listLayout.layoutGroup.length) {
    return listLayout.layoutGroup
  } else {
    return listLayout.layoutAll
  }
}
module.exports = [
  ["get", "/home_v2", homeHandle, {cache: true}],
  ["post", "/home_v2", homeHandle, {cache: true}]
]