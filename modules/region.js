const db = mrequire('./services/tvcdb')
const { isUid } = mrequire('./utils')

const nationwide_local = "0xc353"
const nationwide_dev = "0x7c27"
const nationwide_prod = "0x1f5230"
const nationwide_cloud = "0x2716"

async function getCusRegion (customer_id) {
  if (customer_id && isUid(customer_id)) {
    const {result} = await db.query(`{
      var(func: uid(${customer_id})) @filter(NOT eq(is_deleted, true)) {
        address { province { pv as uid } }
      }
      result(func: uid(pv)) @filter(type(Region)) { uid name }
    }`)
    if (result?.length > 0) {
      return result[0].uid
    } else {
      return null
    }
  } else {
    return null
  }
}

const cus_region_filter_p = `var(func: type(Region)) @filter(uid($customer_region,$nationwide_local,$nationwide_dev,$nationwide_prod,$nationwide_cloud)) {
  ~product.areas {
    p as uid
  }
}`

const region_filter_p = `` //Thể theo yêu cầu mới, thì không cần cái filter này nữa.

// var(func: type(Region)) @filter(uid($nationwide_local,$nationwide_dev,$nationwide_prod)) {
//   ~product.areas {
//     p as uid
//   }
// }
module.exports = {
  nationwide_local,
  nationwide_dev,
  nationwide_prod,
  nationwide_cloud,
  getCusRegion,
  cus_region_filter_p,
  region_filter_p
}