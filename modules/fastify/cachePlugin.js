const { str } = mrequire('./modules/stringHashCode')

const preHandler = {
  GET (req, reply, done) {
    const hash = req.hash = str(req.url).toString(36)
    const cache = this[hash]
    if (cache) {
      // Không cần biết dữ liệu có thay đổi không,
      // nhưng còn trong thời gian cache thì cứ response cache
      if ('if-modified-since' in req.headers) {
        reply.code(304).send() // không gọi done(null, null) ở đây, dể cho body ở onSend thành undefined
      } else {
        reply.type(cache.type).send(cache.body)
      }
    } else {
      req.expired = true
    }
    done()
  },
  POST (req, reply, done) {
    const hash = str(req.url + JSON.stringify(req.body)).toString(36)
    const cache = this[hash]
    if (cache) {
      reply.type(cache.type).send(cache.body)
    } else {
      req.hash = hash
    }
    done()
  }
}

const onSend = {
  GET (req, reply, body, done) {
    if (reply.statusCode === 200 && body) {
      if (req.expired) {
        this[req.hash] = {
          body,
          type: reply.getHeader('content-type'),
          time: (new Date()).toUTCString()
        }
        setTimeout((c, k) => {
          delete c[k]
        }, 15000, this, req.hash)
      }
      reply.header('Cache-Control', 'public, max-age=15')
      reply.header('last-modified', this[req.hash].time)
    }
    done()
  },
  POST (req, reply, body, done) {
    if (reply.statusCode === 200) {
      if (req.hash) {
        this[req.hash] = {
          body,
          type: reply.getHeader('content-type')
        }
        setTimeout((c, k) => {
          delete c[k]
        }, 15000, this, req.hash)
      }
    }
    done()
  }
}

const methods = ['POST', 'GET']

module.exports = function (route) {
  if (route.cache === true && methods.includes(route.method)) {
    const cache = {}
    route.preHandler = preHandler[route.method].bind(cache)
    route.onSend = onSend[route.method].bind(cache)
  }
}