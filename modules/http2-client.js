"use strict"

const http = require("http2")

class Response {
  constructor(headers, text) {
    this.statusCode = headers[":status"]
    this.headers = headers
    this.text = text
  }
  get json() {
    return JSON.parse(this.text)
  }
}

function createHttp2Client (baseUrl) {
  let client = null
  function clean () {
    if(client && !client.closed) {
      client.close()
      client = null
    }
  }
  function createRequest (callback, onError, options) {
    if(client) {
      callback(client.request(options))
    } else {
      http.connect(baseUrl)
        .on('error', err => {
          clean()
          onError(err)
        })
        .on('timeout', clean)
        .on('close', () => {
          client = null
        })
        .on('connect', function (session) {
          client = session
          callback(client.request(options))
        })
    }
  }
  return (uri) => new Promise(function (resolve, reject) {
    const opts = {
      ":path": uri,
      "cookie": "session=aapSuQ3j5HVpIf8i1559gAyh1YlH0JeIw1wy%2BSYrOxBCjHYs3I%2Fw3Fk%2FJXh%2FrdwhHvQ%3D%3BnBop%2FNGMZGRdqR9ee1nyFqlZza5Art%2F8" // Ynr67C1XdY
    }
    createRequest(req => {
      req.setEncoding("utf8")
      req.on("response", (headers) => {
        const contentLength = parseInt(headers["content-length"])
        const buff = Buffer.alloc(contentLength)
        let offset = 0
        req.on("data", (chunk) => {
          offset += buff.write(chunk, offset)
        })
        req.on("end", () => {
          const response = new Response(headers, buff.toString())
          headers[":status"] === 200 ? resolve(response) : reject(response)
        })
      })
    }, reject, opts)
  })
}

module.exports = createHttp2Client