const toPromise = (o, fn) => {
  const func = o[fn].bind(o)
  return (...params) => new Promise((resolve) => {
    func(...params, (err, reply) => {
      if(err) {
        log.error(err)
      }
      resolve(reply)
    })
  })
}

function getRedisClient () {
  const { redis } = config.services
  const client = require("redis").createClient(redis)
  client.on("error", (err) => {
    log.error(err)
  })
  const [get, set, incr] = ['get', 'set', 'incr'].map(fn => toPromise(client, fn))
  return { get, set, incr }
}

function getDevClient () {
  const store = new Map()
  return {
    get (key) {
      return new Promise(function (resolve) {
        resolve(store.get(key))
      })
    },
    set (key, value) {
      return new Promise(function (resolve) {
        store.set(key, value)
        resolve()
      })
    },
    incr (key) {
      return new Promise((resolve) => {
        let v = store.get(key) || 0
        store.set(key, ++v)
        resolve(v)
      })
    }
  }
}

module.exports = process.env.SESSION_TYPE === "redis" ? getRedisClient() : getDevClient()
