const uid = /^0[xX][0-9A-F]+$/i
const orderId = /^S\d{6}-\d{6}$/
const subOrderId = /^S\d{6}-\d{6}[A-Z]{1,2}$/
const macAddr = /^([a-fA-F0-9]{2}:){5}[a-fA-F0-9]{2}$/
const number = /^\d+$/

module.exports = {
  handleUrlDescHtml (str = "", url = "") {
    const reg = / src="([^http]\S+)" /gm
    return str.replace(reg, ` src="${url}/images/products/$1" `);
  },
  MakeID (length) {
    var result = ''
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    var charactersLength = characters.length
    for (var i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength))
    }
    return result
  },
  isUid: str => uid.test(str),
  isOrderId: str => orderId.test(str),
  isSubOrderId: str => subOrderId.test(str),
  isMacAddr: str => macAddr.test(str),
  isNumber: str => number.test(str),
  uid: uid => ({ uid }),
  createUidMap (items, ...props) {
    if(props.length === 0) {
      return items.reduce((o, item) => {
        o[item.uid] = item
        return o
      }, {})
    }
    if(props.length === 1) {
      const prop = props[0]
      return items.reduce((o, item) => {
        o[item.uid] = item[prop]
        return o
      }, {})
    } else {
      return items.reduce((o, item) => {
        const i = {}
        for(let k of props) {
          i[k] = item[k]
        }
        o[item.uid] = i
        return o
      }, {})
    }
  },
  safeParseJson (str = '', v = {}) {
    if(str.charAt(0) === '{') {
      try {
        return JSON.parse(str)
      } catch(err) {
        log.error(err)
        log.error(str.length > 100 ? str.slice(0, 100) + '...' : str)
      }
      return v
    }
    return {}
  },
  parseDate (date) {
    if(!(date instanceof Date)) {
      date = new Date(date)
    }
    const year = String(date.getFullYear())
    const month = String(date.getMonth() + 1).padStart(2, "0")
    const day = String(date.getDate()).padStart(2, "0")
    return `${day}/${month}/${year}`
  }
}